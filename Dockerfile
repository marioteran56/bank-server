FROM python:3.9.7-slim-bullseye

ENV PYTHONUNBUFFERED=1

WORKDIR /app

# Instalar requisitos previos de Oracle
RUN apt-get update && apt-get install -y libaio1 wget unzip && \
    mkdir /opt/oracle

# Descargar el archivo ZIP de Oracle Instant Client
RUN wget https://download.oracle.com/otn_software/linux/instantclient/199000/instantclient-basic-linux.x64-19.9.0.0.0dbru.zip \
    -O /opt/oracle/instantclient-basic-linux.x64-19.9.0.0.0dbru.zip

# Descomprimir el archivo ZIP de Oracle Instant Client
RUN cd /opt/oracle && \
    unzip instantclient-basic-linux.x64-19.9.0.0.0dbru.zip && \
    rm instantclient-basic-linux.x64-19.9.0.0.0dbru.zip && \
    echo /opt/oracle/instantclient_19_9 > /etc/ld.so.conf.d/oracle-instantclient.conf && \
    ldconfig

# Instalar las dependencias de Python necesarias para su aplicación
RUN pip install --no-cache-dir \
    Django==3.2.7 \
    cx-Oracle==8.2.1 \
    djangorestframework==3.13.1 \
    pytz==2021.1 \
    django-cors-headers

# Copiar archivos de código fuente al directorio de trabajo del contenedor
COPY . /app/

# Iniciar el servidor Django
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
