from django.contrib import admin
from bankApp.models import Prestamo, Sucursal

# Register your models here.

admin.site.register(Sucursal)
admin.site.register(Prestamo)