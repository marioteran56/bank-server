from multiprocessing.dummy import connection
from django.http.response import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views import View
from django.db import connection
import json
import re

class SucursalView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def get(self, request, idsucursal=None):
        cursor = connection.cursor()
        try:
            if idsucursal is not None:
                cursor.execute("SELECT * FROM vista_sucursales WHERE idsucursal=%s", [idsucursal])
            else:
                cursor.execute("SELECT * FROM vista_sucursales")
            rows = cursor.fetchall()
            sucursales = []
            for row in rows:
                sucursales.append({'idsucursal': row[0], 'nombresucursal': row[1], 'ciudadsucursal': row[2], 'activos': row[3], 'region': row[4]})
            data = {'message': 'Success', 'sucursales': sucursales}
        except Exception as e:
            data = {'message': 'Error', 'error': str(e)}
        finally:
            cursor.close()
        return JsonResponse(data)

    def post(self, request):
        cursor = connection.cursor()
        try:
            jd = json.loads(request.body)
            cursor.callproc("insertar_sucursal", (jd['idsucursal'], jd['nombresucursal'], jd['ciudadsucursal'], jd['activos'], jd['region']))
            cursor.execute("COMMIT")
            data = {'message': 'Success'}
        except Exception as e:
            print('errr')
            data = {'message': 'Error', 'error': str(e)}
        finally:
            cursor.close()
        return JsonResponse(data)

    def put(self, request, idsucursal):
        cursor = connection.cursor()
        try:
            jd = json.loads(request.body)
            cursor.callproc("actualizar_sucursal", (idsucursal, jd['nombresucursal'], jd['ciudadsucursal'], jd['activos'], jd['region']))
            cursor.execute("COMMIT")
            data = {'message': 'Success'}
        except Exception as e:
            data = {'message': 'Error', 'error': str(e)}
        finally:
            cursor.close()
        return JsonResponse(data)

    def delete(self, request, idsucursal):
        cursor = connection.cursor()
        try:
            cursor.callproc("eliminar_sucursal", params=(idsucursal, ))
            cursor.execute("COMMIT")
            data = {'message': 'Success'}
        except Exception as e:
            data = {'message': 'Error', 'error': str(e)}
        finally:
            cursor.close()
        return JsonResponse(data)

class PrestamoView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, noprestamo=None):
        cursor = connection.cursor()
        try:
            if noprestamo is not None:
                cursor.execute("SELECT * FROM vista_prestamos WHERE noprestamo=%s", [noprestamo])
            else:
                cursor.execute("SELECT * FROM vista_prestamos")
            rows = cursor.fetchall()
            prestamos = []
            for row in rows:
                prestamos.append({'noprestamo': row[0], 'idsucursal': row[1], 'cantidad': row[2]})
            data = {'message': 'Success', 'prestamos': prestamos}
        except Exception as e:
            data = {'message': 'Error', 'error': str(e)}
        finally:
            cursor.close()
        return JsonResponse(data)

    def post(self, request):
        cursor = connection.cursor()
        try:
            jd = json.loads(request.body)
            cursor.callproc("insertar_prestamo", (jd['noprestamo'], jd['idsucursal'], jd['cantidad']))
            cursor.execute("COMMIT")
            data = {'message': 'Success'}
        except Exception as e:
            data = {'message': 'Error', 'error': str(e)}
        finally:
            cursor.close()
        return JsonResponse(data)

    def put(self, request, noprestamo):
        cursor = connection.cursor()
        try:
            jd = json.loads(request.body)
            cursor.callproc("actualizar_prestamo", (noprestamo, jd['idsucursal'], jd['cantidad']))
            cursor.execute("COMMIT")
            data = {'message': 'Success'}
        except Exception as e:
            data = {'message': 'Error', 'error': str(e)}
        finally:
            cursor.close()
        return JsonResponse(data)

    def delete(self, request, noprestamo):
        cursor = connection.cursor()
        try:
            cursor.callproc("eliminar_prestamo", params=(noprestamo, ))
            cursor.execute("COMMIT")
            data = {'message': 'Success'}
        except Exception as e:
            data = {'message': 'Error', 'error': str(e)}
        finally:
            cursor.close()
        return JsonResponse(data)
