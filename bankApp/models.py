from django.db import models

class Sucursal(models.Model):
    idsucursal = models.CharField(max_length=5, primary_key=True)
    nombresucursal = models.CharField(max_length=15, null=False)
    ciudadsucursal = models.CharField(max_length=15, null=False)
    activos = models.IntegerField(null=False)
    region = models.IntegerField(null=True)

    class Meta:
        managed = False
        db_table = 'sucursal'


class Prestamo(models.Model):
    noprestamo = models.CharField(max_length=15, primary_key=True)
    idsucursal = models.ForeignKey(Sucursal, models.DO_NOTHING, db_column='idsucursal', null=False)
    cantidad = models.IntegerField(null=False)

    class Meta:
        managed = False
        db_table = 'prestamo'
