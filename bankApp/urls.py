from django.urls import path
from .views import SucursalView, PrestamoView

urlpatterns = [
    path('sucursales/', SucursalView.as_view(), name='sucursal_list'),
    path('sucursales/<str:idsucursal>/', SucursalView.as_view(), name='sucursal_detail'),
    path('prestamos/', PrestamoView.as_view(), name='prestamo_list'),
    path('prestamos/<str:noprestamo>/', PrestamoView.as_view(), name='prestamo_detail'),
]
